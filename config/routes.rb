Rails.application.routes.draw do
  devise_for :users
  get 'about' => 'movies#about'
  resources :movies
  resources :genres, only: :show
  get 'movies/recommended/:placement' => 'movies#recommended'
  root 'movies#index'
end
