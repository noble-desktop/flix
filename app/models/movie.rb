class Movie < ApplicationRecord
  validates :title, :mpaa_rating, :runtime, :poster_image, presence: true
  validates :runtime, numericality: true

  has_many :cast_members
  belongs_to :genre, optional: true

  scope :with_placement, -> (placement) { where(placement: placement) }

  def self.all_mpaa_ratings
    %w(G PG R NR)
  end

  validates :mpaa_rating, inclusion: { in: self.all_mpaa_ratings }

  def runtime_hours
    unless runtime.nil?
      "#{runtime / 60} hrs. #{runtime % 60} min."
    end
  end

  def cast
    cast_members.map { |c| c.name }.join(', ') unless cast_members.nil?
  end
end
